/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gonza
 */
@Entity
@Table(name = "pro_proveedores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProProveedores.findAll", query = "SELECT p FROM ProProveedores p"),
    @NamedQuery(name = "ProProveedores.findByProIdproveedor", query = "SELECT p FROM ProProveedores p WHERE p.proIdproveedor = :proIdproveedor"),
    @NamedQuery(name = "ProProveedores.findByProRutproovedor", query = "SELECT p FROM ProProveedores p WHERE p.proRutproovedor = :proRutproovedor"),
    @NamedQuery(name = "ProProveedores.findByProNomproveedor", query = "SELECT p FROM ProProveedores p WHERE p.proNomproveedor = :proNomproveedor"),
    @NamedQuery(name = "ProProveedores.findByProRazonsocial", query = "SELECT p FROM ProProveedores p WHERE p.proRazonsocial = :proRazonsocial"),
    @NamedQuery(name = "ProProveedores.findByProRepresentante", query = "SELECT p FROM ProProveedores p WHERE p.proRepresentante = :proRepresentante"),
    @NamedQuery(name = "ProProveedores.findByProRutrepresentante", query = "SELECT p FROM ProProveedores p WHERE p.proRutrepresentante = :proRutrepresentante")})
public class ProProveedores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "pro_idproveedor")
    private String proIdproveedor;
    @Size(max = 15)
    @Column(name = "pro_rutproovedor")
    private String proRutproovedor;
    @Size(max = 50)
    @Column(name = "pro_nomproveedor")
    private String proNomproveedor;
    @Size(max = 50)
    @Column(name = "pro_razonsocial")
    private String proRazonsocial;
    @Size(max = 50)
    @Column(name = "pro_representante")
    private String proRepresentante;
    @Size(max = 50)
    @Column(name = "pro_rutrepresentante")
    private String proRutrepresentante;

    public ProProveedores() {
    }

    public ProProveedores(String proIdproveedor) {
        this.proIdproveedor = proIdproveedor;
    }

    public String getProIdproveedor() {
        return proIdproveedor;
    }

    public void setProIdproveedor(String proIdproveedor) {
        this.proIdproveedor = proIdproveedor;
    }

    public String getProRutproovedor() {
        return proRutproovedor;
    }

    public void setProRutproovedor(String proRutproovedor) {
        this.proRutproovedor = proRutproovedor;
    }

    public String getProNomproveedor() {
        return proNomproveedor;
    }

    public void setProNomproveedor(String proNomproveedor) {
        this.proNomproveedor = proNomproveedor;
    }

    public String getProRazonsocial() {
        return proRazonsocial;
    }

    public void setProRazonsocial(String proRazonsocial) {
        this.proRazonsocial = proRazonsocial;
    }

    public String getProRepresentante() {
        return proRepresentante;
    }

    public void setProRepresentante(String proRepresentante) {
        this.proRepresentante = proRepresentante;
    }

    public String getProRutrepresentante() {
        return proRutrepresentante;
    }

    public void setProRutrepresentante(String proRutrepresentante) {
        this.proRutrepresentante = proRutrepresentante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proIdproveedor != null ? proIdproveedor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProProveedores)) {
            return false;
        }
        ProProveedores other = (ProProveedores) object;
        if ((this.proIdproveedor == null && other.proIdproveedor != null) || (this.proIdproveedor != null && !this.proIdproveedor.equals(other.proIdproveedor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.ProProveedores[ proIdproveedor=" + proIdproveedor + " ]";
    }
    
}
