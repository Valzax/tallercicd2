package root.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.persistence.entities.ProProveedores;

@Path("/proveedor")
public class ProveedoresRest {
    
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("Proveedores_PU");
    EntityManager em;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {
        em = emf.createEntityManager();
        List<ProProveedores> lista = em.createNamedQuery("ProProveedores.findAll").getResultList();
        return Response.ok(200).entity(lista).build();
    }
    
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarPorID(@PathParam("idbuscar") String idbuscar){
        em = emf.createEntityManager();
        ProProveedores proveedor = em.find(ProProveedores.class, idbuscar);
        return Response.ok(200).entity(proveedor).build(); 
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String nuevo(ProProveedores nuevoProveedor) {
        em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(nuevoProveedor);
        em.getTransaction().commit();
        return "Proveedor Guardado";
    }
    
    @PUT
    public Response actualizar(ProProveedores updateProveedor) {
        em = emf.createEntityManager();
        em.getTransaction().begin();
        updateProveedor = em.merge(updateProveedor);
        em.getTransaction().commit();
        return Response.ok(updateProveedor).build();
    }
    
    @DELETE
    @Path("/{iddelete}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
    public Response eliminaPorID(@PathParam("iddelete") String iddelete){
        em = emf.createEntityManager();
        em.getTransaction().begin();
        ProProveedores deleteProv = em.getReference(ProProveedores.class, iddelete);
        em.remove(deleteProv);
        em.getTransaction().commit();
        return Response.ok("CLiente Eliminado").build();
    }
}
